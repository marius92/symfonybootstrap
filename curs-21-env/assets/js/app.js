/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');
import Vue from 'vue';
var jsapp = new Vue({
    el: '#js-app',
    delimiters: ['{*', '*}'],
    data: {
        message: 'Hello World',
        title: 'Titlu Static',
        title1: 'titlu 1',
        title2: 'titlu 2',
        st: true,
    },
    methods: {
        changeText: function () {
            this.message = "Buna 2";
            this.title = "Un alt titlu";
        },
        schimbaTitlu: function () {
            // if (this.st)
            //     this.st = false
            // else
            //     this.st = true
            this.st = !this.st;

        }
    }

});



console.log('Hello Webpack Encore! Edit me in assets/js/app.js');