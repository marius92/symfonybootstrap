<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShowDateController extends AbstractController
{
    /**
     * @Route("/showdate", name="show_date")
     */
    public function index()
    {
        $today = date('d-m-Y H:i:s');
        return $this->render('show_date/index.html.twig', [
            'controller_name' => 'Bootstrap',
            'today' => $today
        ]);
    }
}
